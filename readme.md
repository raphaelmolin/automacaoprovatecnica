# Automação de Testes da Prova técnica API - SICREDI
# Desenvolvido por Raphael Molin

O projeto de automação desenvolvido para atender os requisitos propostos no desafio utilizou o framework Cucumber, com o qual foram criadas as features da automação (../src/test/java/br/com/sicredi/tests/features). Nessas features constam todos os cenários de cobertura de testes, onde cada cenário possui uma tag para identificação na execução da automação.

Para a execução dos testes foi criada uma classe exclusiva para gerenciar quais features serão executadas (ExecutaAutomacao.java).
Nessa classe, é possível definir qual tag (cenário) será executado, informando-a no parâmetro "tags":

@CucumberOptions(features = "src/",
                 plugin = { "json", "html:reports/cucumber-html-report","json:reports/cucumber-html-report.json"},
                 tags = {"@Todos"}
                 )

Ao utilizar a tag "@Todos", a execução considerará todos os cenários criados na package features.
Abaixo elenco todas as tags criadas para utilização dos testes:

1. @Todos >> considera todos os cenário de todas as features
2. @Restricoes >> considera todos os cenários das features do cadastro de requisições
3. @RestricoesCenarioA >> considera o cenário: Realizar consulta de um CPF para identificar se possui restrição
4. @Simulacoes >> considera todos os cenários das features do cadastro de simulações
5. @CadastrarSimulacoes >> considera todos os cenários das features de inclusão de simulações
6. @SimulacoesPostCenarioA >> considera o cenário: Realizar o cadastro de uma simulação com CPF sem restrição
7. @SimulacoesPostCenarioB >> considera o cenário: Realizar o cadastro de uma simulação com CPF com restrição
8. @SimulacoesPostCenarioC >> considera o cenário: Validar a inclusão de simulação sem informação no campo nome
9. @SimulacoesPostCenarioD >> considera o cenário: Validar a inclusão de simulação sem informação no campo cpf
10. @SimulacoesPostCenarioE >> considera o cenário: Validar a inclusão de simulação sem informação no campo email
11. @SimulacoesPostCenarioF >> considera o cenário: Validar a inclusão de simulação sem informação no campo valor
12. @SimulacoesPostCenarioG >> considera o cenário: Validar a inclusão de simulação sem informação no campo parcelas
13. @SimulacoesPostCenarioH >> considera o cenário: Validar a inclusão de simulação com valor inferior a R$ 1000,00
14. @SimulacoesPostCenarioI >> considera o cenário: Validar a inclusão de simulação com valor superior a R$ 40000,00
15. @SimulacoesPostCenarioJ >> considera o cenário: Validar a inclusão de simulação com email inválido
16. @SimulacoesPostCenarioK >> considera o cenário: Validar a inclusão de simulação com número de parcela inferior a 2
17. @SimulacoesPostCenarioL >> considera o cenário: Validar a inclusão de simulação com número de parcela superior a 48
18. @ConsultaSimulacoes >> considera todos os cenários das features de consulta de simulações
19. @SimulacoesGetAllCenarioA >> considera o cenário: Realizar consulta de todas as simulações cadastradas
20. @SimulacaoGetCenarioA >> considera o cenário: Realizar consulta de uma simulação pelo CPF
21. @SimulacaoPutCenarioA >> considera o cenário: Realizar a alteração de uma simulação
22. @SimulacaoDeleteCenarioA >> considera o cenário: Realizar remoção de uma solicitação

## Como executar a automação 

O projeto foi desenvolvido com a ferramenta IntelliJ. Nela foi criada uma configuração na tela Run/Debug Configurations (Run >> Edit Configuration...).
Segue passos para criação de uma nova configuração:
1. Acessar a tela Run/Debug Configurations
2. Clicar no ícone "+" >> Add New Configuration (Alt + Insert)
3. Clicar sobre a opção JUnit
4. No campo Name, informar qualquer nome para configuração
5. Informar no campo Class da aba Configuration a informação: br.com.sicredi.tests.ExecutaAutomacao
6. Na aba Code Coverage, em Packages and classes to record coverage data, clicar no botão + >> Add Package e selecionar o caminho br.com.sicredi.tests
7. Clicar no botão Ok

Após adicionar a configuração, basta clicar em Run >> Run... (Alt + Shift + F10) e selecionar a configuração que foi criada.

## Repositório

Ao concluir a execução da automação, na pasta reports >> cucumber-html-report será criado o arquivo index.html, onde consta informações da execução da automação.
Quando o cenário possuir a cor de fundo verde, indica que o teste passou.
Quando o cenário possuir a cor de fundo vermelha, indica que o teste falhou apresentando o erro apresentado.
