$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/br/com/sicredi/tests/features/#1_ConsultarRestricaoCPF.feature");
formatter.feature({
  "name": "Consultar um CPF",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.step({
  "name": "o usuário consultou se o CPF \"\u003cCPF\u003e\" possui restrição",
  "keyword": "Given "
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CPF",
        "mensagem"
      ]
    },
    {
      "cells": [
        "97093236014",
        "O CPF 97093236014 tem problema"
      ]
    },
    {
      "cells": [
        "60094146012",
        "O CPF 60094146012 tem problema"
      ]
    },
    {
      "cells": [
        "84809766080",
        "O CPF 84809766080 tem problema"
      ]
    },
    {
      "cells": [
        "62648716050",
        "O CPF 62648716050 tem problema"
      ]
    },
    {
      "cells": [
        "26276298085",
        "O CPF 26276298085 tem problema"
      ]
    },
    {
      "cells": [
        "01317496094",
        "O CPF 01317496094 tem problema"
      ]
    },
    {
      "cells": [
        "55856777050",
        "O CPF 55856777050 tem problema"
      ]
    },
    {
      "cells": [
        "19626829001",
        "O CPF 19626829001 tem problema"
      ]
    },
    {
      "cells": [
        "24094592008",
        "O CPF 24094592008 tem problema"
      ]
    },
    {
      "cells": [
        "58063164083",
        "O CPF 58063164083 tem problema"
      ]
    },
    {
      "cells": [
        "11111111111",
        ""
      ]
    },
    {
      "cells": [
        "1111111",
        ""
      ]
    },
    {
      "cells": [
        "ABC",
        ""
      ]
    }
  ]
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"97093236014\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 97093236014 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"60094146012\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 60094146012 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"84809766080\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 84809766080 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"62648716050\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 62648716050 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"26276298085\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 26276298085 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"01317496094\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 01317496094 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"55856777050\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 55856777050 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"19626829001\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 19626829001 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"24094592008\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 24094592008 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"58063164083\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"O CPF 58063164083 tem problema\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"11111111111\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"1111111\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de um CPF para identificar se possui restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Restricoes"
    },
    {
      "name": "@RestricoesCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"ABC\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a mensagem \"\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosRestricoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("src/test/java/br/com/sicredi/tests/features/#2_CadastrarSimulacao.feature");
formatter.feature({
  "name": "Cadastrar uma simulação",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Realizar o cadastro de uma simulação com CPF sem restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioA"
    }
  ]
});
formatter.step({
  "name": "o usuário consultou se o CPF \"\u003cCPF\u003e\" possui restrição",
  "keyword": "Given "
});
formatter.step({
  "name": "o CPF não possua restrição",
  "keyword": "And "
});
formatter.step({
  "name": "realizar a inclusão da simulação",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    },
    {
      "cells": [
        "Cliente B",
        "00000000272",
        "clienteb@teste.com",
        "10000.00",
        "48",
        "false"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação do CPF \"\u003cCPF\u003e\" será incluída com sucesso",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CPF"
      ]
    },
    {
      "cells": [
        "00000000191"
      ]
    },
    {
      "cells": [
        "00000000272"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Realizar o cadastro de uma simulação com CPF sem restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"00000000191\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o CPF não possua restrição",
  "keyword": "And "
});
formatter.match({
  "location": "PassosProjeto.semRestricaoCPF()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "realizar a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    },
    {
      "cells": [
        "Cliente B",
        "00000000272",
        "clienteb@teste.com",
        "10000.00",
        "48",
        "false"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "PassosSimulacoes.realizarInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação do CPF \"00000000191\" será incluída com sucesso",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.simulacaoRetornaStatus(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar o cadastro de uma simulação com CPF sem restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultou se o CPF \"00000000272\" possui restrição",
  "keyword": "Given "
});
formatter.match({
  "location": "PassosProjeto.consultouRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o CPF não possua restrição",
  "keyword": "And "
});
formatter.match({
  "location": "PassosProjeto.semRestricaoCPF()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "realizar a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    },
    {
      "cells": [
        "Cliente B",
        "00000000272",
        "clienteb@teste.com",
        "10000.00",
        "48",
        "false"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "PassosSimulacoes.realizarInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação do CPF \"00000000272\" será incluída com sucesso",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.simulacaoRetornaStatus(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Realizar o cadastro de uma simulação com CPF com restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioB"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente com restrição",
        "97093236014",
        "clienterestricao@teste.com",
        "5000.00",
        "2",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "o CPF \"\u003cCPF\u003e\" possua restrição",
  "keyword": "And "
});
formatter.step({
  "name": "a simulação não deve incluir o CPF \"\u003cCPF\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CPF"
      ]
    },
    {
      "cells": [
        "97093236014"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Realizar o cadastro de uma simulação com CPF com restrição",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioB"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente com restrição",
        "97093236014",
        "clienterestricao@teste.com",
        "5000.00",
        "2",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o CPF \"97093236014\" possua restrição",
  "keyword": "And "
});
formatter.match({
  "location": "PassosProjeto.comRestricaoCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não deve incluir o CPF \"97093236014\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.simulacaoNaoDeveIncluirCPF(String)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: O sistema permitiu incluir uma simulação com CPF com restrição\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat br.com.sicredi.tests.core.passos.PassosSimulacoes.simulacaoNaoDeveIncluirCPF(PassosSimulacoes.java:91)\r\n\tat ✽.a simulação não deve incluir o CPF \"97093236014\"(src/test/java/br/com/sicredi/tests/features/#2_CadastrarSimulacao.feature:28)\r\n",
  "status": "failed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação sem informação no campo nome",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioC"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"nome\":\"Nome não pode ser vazio\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação sem informação no campo nome",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioC"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"nome\":\"Nome não pode ser vazio\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação sem informação no campo cpf",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioD"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"cpf\":\"CPF não pode ser vazio\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação sem informação no campo cpf",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioD"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"cpf\":\"CPF não pode ser vazio\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação sem informação no campo email",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioE"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "",
        "1000.00",
        "2",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"email\":\"E-mail não deve ser vazio\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação sem informação no campo email",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioE"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "",
        "1000.00",
        "2",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"email\":\"E-mail não deve ser vazio\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação sem informação no campo valor",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioF"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "",
        "2",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"valor\":\"Valor não pode ser vazio\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação sem informação no campo valor",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioF"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "",
        "2",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"valor\":\"Valor não pode ser vazio\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação sem informação no campo parcelas",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioG"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"parcelas\":\"Parcelas não pode ser vazio\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação sem informação no campo parcelas",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioG"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"parcelas\":\"Parcelas não pode ser vazio\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação com valor inferior a R$ 1000,00",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioH"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "100.00",
        "2",
        "false"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"valor\":\"Valor deve ser maior ou igual a R$ 1.000\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação com valor inferior a R$ 1000,00",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioH"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "100.00",
        "2",
        "false"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"valor\":\"Valor deve ser maior ou igual a R$ 1.000\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Registro foi inserido indevidamente. Mensagem esperada: {\"erros\":{\"valor\":\"Valor deve ser maior ou igual a R$ 1.000\"}}\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat br.com.sicredi.tests.core.passos.PassosSimulacoes.retornarMensagem(PassosSimulacoes.java:107)\r\n\tat ✽.a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"valor\":\"Valor deve ser maior ou igual a R$ 1.000\"}}\"(src/test/java/br/com/sicredi/tests/features/#2_CadastrarSimulacao.feature:100)\r\n",
  "status": "failed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação com valor superior a R$ 40000,00",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioI"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "50000.00",
        "2",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"valor\":\"Valor deve ser menor ou igual a R$ 40.000\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação com valor superior a R$ 40000,00",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioI"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "50000.00",
        "2",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"valor\":\"Valor deve ser menor ou igual a R$ 40.000\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação com email inválido",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioJ"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "emailinvalido",
        "5000.00",
        "2",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"email\":\"E-mail deve ser um e-mail válido\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação com email inválido",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioJ"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "emailinvalido",
        "5000.00",
        "2",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"email\":\"E-mail deve ser um e-mail válido\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação com número de parcela inferior a 2",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioK"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "5000.00",
        "1",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"parcelas\":\"Parcelas deve ser igual ou maior que 2\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação com número de parcela inferior a 2",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioK"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "5000.00",
        "1",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"parcelas\":\"Parcelas deve ser igual ou maior que 2\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Validar a inclusão de simulação com número de parcela superior a 48",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacoesPostCenarioL"
    }
  ]
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "5000.00",
        "50",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "mensagem"
      ]
    },
    {
      "cells": [
        "{\"erros\":{\"parcelas\":\"Parcelas deve ser igual ou menor que 48\"}}"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validar a inclusão de simulação com número de parcela superior a 48",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@CadastrarSimulacoes"
    },
    {
      "name": "@SimulacoesPostCenarioL"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "realizou a inclusão da simulação",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "5000.00",
        "50",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.realizouInclusaoSimulacao(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"parcelas\":\"Parcelas deve ser igual ou menor que 48\"}}\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.retornarMensagem(String)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Registro foi inserido indevidamente. Mensagem esperada: {\"erros\":{\"parcelas\":\"Parcelas deve ser igual ou menor que 48\"}}\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat br.com.sicredi.tests.core.passos.PassosSimulacoes.retornarMensagem(PassosSimulacoes.java:107)\r\n\tat ✽.a simulação não será inserida e apresentará a mensagem \"{\"erros\":{\"parcelas\":\"Parcelas deve ser igual ou menor que 48\"}}\"(src/test/java/br/com/sicredi/tests/features/#2_CadastrarSimulacao.feature:148)\r\n",
  "status": "failed"
});
formatter.uri("src/test/java/br/com/sicredi/tests/features/#3_ConsultarTodasSimulacoes.feature");
formatter.feature({
  "name": "Consultar todas as simulações",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@ConsultaSimulacoes"
    }
  ]
});
formatter.scenario({
  "name": "Realizar consulta de todas as simulações cadastradas",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@ConsultaSimulacoes"
    },
    {
      "name": "@SimulacoesGetAllCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "existam as simulações",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    },
    {
      "cells": [
        "Cliente B",
        "00000000272",
        "clienteb@teste.com",
        "10000.00",
        "48",
        "false"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.existamSimulacoes(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultar todas as simulações",
  "keyword": "When "
});
formatter.match({
  "location": "PassosSimulacoes.consultarTodasSimulacoes()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar as simulações cadastradas",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.deveRetornarTodasSimulacoesCadastradas()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("src/test/java/br/com/sicredi/tests/features/#4_ConsultarSimulacao.feature");
formatter.feature({
  "name": "Consultar simulação pelo CPF",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@ConsultaSimulacoes"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Realizar consulta de uma simulação pelo CPF",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacaoGetCenarioA"
    }
  ]
});
formatter.step({
  "name": "existam as simulações",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    },
    {
      "cells": [
        "Cliente B",
        "00000000272",
        "clienteb@teste.com",
        "10000.00",
        "48",
        "false"
      ]
    }
  ]
});
formatter.step({
  "name": "o usuário consultar o CPF \"\u003cCPF\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "o serviço deverá retornar a simulação cadastrada para o CPF \"\u003cCPF\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CPF"
      ]
    },
    {
      "cells": [
        "00000000191"
      ]
    },
    {
      "cells": [
        "00000000272"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Realizar consulta de uma simulação pelo CPF",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@ConsultaSimulacoes"
    },
    {
      "name": "@SimulacaoGetCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "existam as simulações",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    },
    {
      "cells": [
        "Cliente B",
        "00000000272",
        "clienteb@teste.com",
        "10000.00",
        "48",
        "false"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.existamSimulacoes(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultar o CPF \"00000000191\"",
  "keyword": "When "
});
formatter.match({
  "location": "PassosSimulacoes.consultarSimulacaoPorCpf(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a simulação cadastrada para o CPF \"00000000191\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.deveRetornarSimulacaoDoCpf(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Realizar consulta de uma simulação pelo CPF",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@ConsultaSimulacoes"
    },
    {
      "name": "@SimulacaoGetCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "existam as simulações",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    },
    {
      "cells": [
        "Cliente B",
        "00000000272",
        "clienteb@teste.com",
        "10000.00",
        "48",
        "false"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.existamSimulacoes(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o usuário consultar o CPF \"00000000272\"",
  "keyword": "When "
});
formatter.match({
  "location": "PassosSimulacoes.consultarSimulacaoPorCpf(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o serviço deverá retornar a simulação cadastrada para o CPF \"00000000272\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.deveRetornarSimulacaoDoCpf(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("src/test/java/br/com/sicredi/tests/features/#5_AlterarSimulacao.feature");
formatter.feature({
  "name": "Alterar uma simulação",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Realizar a alteração de uma simulação",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacaoPutCenarioA"
    }
  ]
});
formatter.step({
  "name": "existam as simulações",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "o usuário alterar a simulação do CPF \"\u003cCPF\u003e\"",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A Alterado",
        "00000000191",
        "clientealterado@teste.com",
        "5000.00",
        "12",
        "false"
      ]
    }
  ]
});
formatter.step({
  "name": "a simulação do CPF \"\u003cCPF\u003e\" deverá ser alterada com sucesso",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CPF"
      ]
    },
    {
      "cells": [
        "00000000191"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Realizar a alteração de uma simulação",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@SimulacaoPutCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "existam as simulações",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.existamSimulacoes(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o usuário alterar a simulação do CPF \"00000000191\"",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A Alterado",
        "00000000191",
        "clientealterado@teste.com",
        "5000.00",
        "12",
        "false"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "PassosSimulacoes.alterarCampoSimulacao(String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação do CPF \"00000000191\" deverá ser alterada com sucesso",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.deveAlterarSimulacao(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("src/test/java/br/com/sicredi/tests/features/#6_RemoverSimulacao.feature");
formatter.feature({
  "name": "Remover uma simulação",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Realizar remoção de uma solicitação",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SimulacaoDeleteCenarioA"
    }
  ]
});
formatter.step({
  "name": "existam as simulações",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    }
  ]
});
formatter.step({
  "name": "o usuário solicitar a remoção da simulação do CPF \"\u003cCPF\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "a simulação do CPF \"\u003cCPF\u003e\" deverá ser removida com sucesso",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CPF"
      ]
    },
    {
      "cells": [
        "00000000191"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Realizar remoção de uma solicitação",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Todos"
    },
    {
      "name": "@Simulacoes"
    },
    {
      "name": "@SimulacaoDeleteCenarioA"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "existam as simulações",
  "rows": [
    {
      "cells": [
        "nome",
        "cpf",
        "email",
        "valor",
        "parcelas",
        "seguro"
      ]
    },
    {
      "cells": [
        "Cliente A",
        "00000000191",
        "clientea@teste.com",
        "1000.00",
        "2",
        "true"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "PassosSimulacoes.existamSimulacoes(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o usuário solicitar a remoção da simulação do CPF \"00000000191\"",
  "keyword": "When "
});
formatter.match({
  "location": "PassosSimulacoes.removerSimulacaoPeloCPF(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a simulação do CPF \"00000000191\" deverá ser removida com sucesso",
  "keyword": "Then "
});
formatter.match({
  "location": "PassosSimulacoes.deveRemoverSimulacao(String)"
});
formatter.result({
  "status": "passed"
});
});