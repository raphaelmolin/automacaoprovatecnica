package br.com.sicredi.tests;

import br.com.sicredi.tests.core.configuracao.Auxiliar;
import br.com.sicredi.tests.core.configuracao.IniciaAutomacao;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Cucumber.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CucumberOptions(features = "src/",
                 plugin = { "json", "html:reports/cucumber-html-report","json:reports/cucumber-html-report.json"},
                 tags = {"@Todos"}
                 )

public class ExecutaAutomacao {

    protected static Auxiliar auxiliar;

    public ExecutaAutomacao() {
    }

    @BeforeClass
    public static void iniciaAutomacao() {
        IniciaAutomacao.inicializarAutomacao();
    }

}
