package br.com.sicredi.tests.core.passos;

import br.com.sicredi.tests.ExecutaAutomacao;
import br.com.sicredi.tests.core.configuracao.Notificacoes;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class PassosRestricoes extends ExecutaAutomacao {

    private final PassosProjeto passosProjeto;

    public PassosRestricoes() {
        passosProjeto = new PassosProjeto();
    }

    @Before(value = "@Restricoes")
    public void before(Scenario cenario) {
        auxiliar.setCenario(cenario);
        PassosProjeto.nomeCadastro = "Restrições";
    }

    @Then("^o serviço deverá retornar a mensagem \"(.*?)\"$")
    public void retornarMensagem(String filtro){
        if (filtro.equals(passosProjeto.mensagemRetornada)) {
            Notificacoes.addLogConsole("Mensagem retornada: " + passosProjeto.mensagemRetornada);
            Notificacoes.addLogConsole("*******************************************************************");
        } else {
            Assert.fail("Mensagem esperada: " + filtro + " - Mensagem recebida: " + passosProjeto.mensagemRetornada);
        }
    }

}
