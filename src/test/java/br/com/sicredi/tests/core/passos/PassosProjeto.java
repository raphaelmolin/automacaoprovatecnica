package br.com.sicredi.tests.core.passos;

import br.com.sicredi.tests.ExecutaAutomacao;
import br.com.sicredi.tests.core.configuracao.Notificacoes;
import br.com.sicredi.tests.core.interfaces.IServicos;
import br.com.sicredi.tests.core.servicos.ServicosRestricoes;
import br.com.sicredi.tests.core.servicos.ServicosSimulacoes;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.And;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PassosProjeto extends ExecutaAutomacao {

    public static String nomeCadastro;
    public static String mensagemRetornada;
    public static String statusRetorno;
    public static String idRetornado;
    public static String cpfRetornado;
    public static Boolean possuiRestricao;
    public static String simulacoesCadastradas;
    private static final String GET = "Realizando consulta de ";
    private static final String POST = "Realizando inclusão de ";
    private static final String PUT = "Realizando atualização de ";
    private static final String DELETE = "Realizando exclusão de ";

    private static Map<String, IServicos> registros = new HashMap<String, IServicos>();
    static {
        registros.put("Restrições", new ServicosRestricoes());
        registros.put("Simulações", new ServicosSimulacoes());
    }

    @Given("^o usuário consultou se o CPF \"(.*?)\" possui restrição$")
    public void consultouRestricaoCPF(String filtro){
        consultarRestricaoCPF("Restrições", filtro);
    }

    @And("^o CPF não possua restrição$")
    public void semRestricaoCPF(){
        if (!possuiRestricao) {
            Notificacoes.addLogConsole("O CPF não possui restrição!");
        } else {
            Assert.fail("O CPF possui restrição!");
        }
    }

    @And("^o CPF \"(.*?)\" possua restrição$")
    public void comRestricaoCPF(String filtro){
        consultarRestricaoCPF("Restrições", filtro);

        if (possuiRestricao) {
            Notificacoes.addLogConsole("O CPF possui restrição!");
        } else {
            Assert.fail("O CPF não possui restrição!");
        }
    }

    public final void consultarRestricaoCPF(String cadastro, String identificador) {
        if (auxiliar.getNumeroCenarios() == 1) {
            Notificacoes.addLogConsole(GET.concat(cadastro));

            mensagemRetornada = "";
            possuiRestricao = null;

            try {
                registros.get(cadastro).getApiRest(identificador);
            } catch (Exception e) {
                Assert.fail("Não foi possível realizar a consulta de " + cadastro);
            }

            Notificacoes.addLogConsole("Consulta do CPF ".concat(identificador).concat(" no cadastro ").concat(cadastro).concat(" concluída"));
        }
    }

    public final void incluirSimulacao(DataTable tabela) {
        if (auxiliar.getNumeroCenarios() == 1) {
            Notificacoes.addLogConsole(POST.concat(nomeCadastro));
            List<List<String>> listaTab = tabela.raw();

            statusRetorno = "";

            try {
                registros.get(nomeCadastro).postApiRest(listaTab);
            } catch (Exception e) {
                Assert.fail("Não foi possível realizar a inclusão da simulação");
            }

            if (statusRetorno.equals(201)) {
                Notificacoes.addLogConsole("Inclusão da simulação concluída");
            } else {
                Notificacoes.addLogConsole("Não foi possível incluir a simulação. Mensagem retornada: " + mensagemRetornada);
            }

            Notificacoes.addLogConsole("*******************************************************************");
        }
    }

    public final void alterarSimulacao(String identificador, DataTable tabela) {
        if (auxiliar.getNumeroCenarios() == 1) {
            Notificacoes.addLogConsole(PUT.concat(nomeCadastro));
            List<List<String>> listaTab = tabela.raw();

            statusRetorno = "";

            try {
                registros.get(nomeCadastro).putApiRest(identificador, listaTab);
            } catch (Exception e) {
                Assert.fail("Não foi possível realizar a inclusão da simulação");
            }

            Notificacoes.addLogConsole("Alteração da simulação concluída");
            Notificacoes.addLogConsole("*******************************************************************");
        }
    }

    public final void consultarSimulacao(String identificador) {
        if (auxiliar.getNumeroCenarios() == 1) {
            Notificacoes.addLogConsole(GET.concat(nomeCadastro));

            try {
                registros.get(nomeCadastro).getApiRest(identificador);
            } catch (Exception e) {
                Assert.fail("Não foi possível realizar a consulta de " + nomeCadastro);
            }

            Notificacoes.addLogConsole("Consulta da simulação do CPF " + identificador + " concluída");
            Notificacoes.addLogConsole("*******************************************************************");
        }
    }

    public final void consultarSimulacoes() {
        if (auxiliar.getNumeroCenarios() == 1) {
            simulacoesCadastradas = null;
            try {
                registros.get(nomeCadastro).getAllApiRest();
            } catch (Exception e) {
                Assert.fail("Não foi possível realizar a consulta de " + nomeCadastro);
            }
        }
    }

    public final void removerSimulacao(String identificador) {
        if (auxiliar.getNumeroCenarios() == 1) {
            Notificacoes.addLogConsole(DELETE.concat(nomeCadastro));

            idRetornado = "";
            mensagemRetornada = "";
            consultarSimulacao(identificador);

            if (!idRetornado.equals("")) {
                try {
                    registros.get(nomeCadastro).deleteApiRest(idRetornado);
                } catch (Exception e) {
                    Assert.fail("Não foi possível realizar a exclusão do ID " + idRetornado + " de " + nomeCadastro);
                }
                Notificacoes.addLogConsole("ID " + idRetornado + " do cadastro de " + nomeCadastro + " excluído com sucesso");
                Notificacoes.addLogConsole("*******************************************************************");
            }
        }
    }

    public final void limparSimulacoes(DataTable tabela) {
        if (auxiliar.getNumeroCenarios() == 1) {
            Notificacoes.addLogConsole(DELETE.concat(nomeCadastro));

            List<List<String>> listaTab = tabela.raw();

            for(int i = 1;i<listaTab.size();i++) {
                String cpf = listaTab.get(i).get(1);

                idRetornado = "";
                mensagemRetornada = "";
                consultarSimulacao(cpf);

                if (!idRetornado.equals("")) {
                    try {
                        registros.get(nomeCadastro).deleteApiRest(idRetornado);
                    } catch (Exception e) {
                        Assert.fail("Não foi possível realizar a exclusão do ID " + idRetornado + " de " + nomeCadastro);
                    }

                    Notificacoes.addLogConsole("ID " + idRetornado + " do cadastro de " + nomeCadastro + " excluído com sucesso");
                    Notificacoes.addLogConsole("*******************************************************************");
                }
            }

        }
    }

    public final void limparSimulacoes() {
        if (auxiliar.getNumeroCenarios() == 1) {
            Notificacoes.addLogConsole(DELETE.concat(nomeCadastro));
            consultarSimulacoes();

            JSONArray arrayJSON = new JSONArray(simulacoesCadastradas);
            for(int i = 0; i < arrayJSON.length(); i++){
                idRetornado = "";

                JSONObject simulacao = arrayJSON.getJSONObject(i);
                simulacao.keySet().forEach(keyStr ->	{
                    Object keyvalue = simulacao.get(keyStr);
                    if (keyStr.equals("id")) {
                        PassosProjeto.idRetornado = keyvalue.toString();
                    }
                    if (!idRetornado.equals("")) {
                        try {
                            registros.get(nomeCadastro).deleteApiRest(idRetornado);
                        } catch (Exception e) {
                            Assert.fail("Não foi possível realizar a exclusão do ID " + idRetornado + " de " + nomeCadastro);
                        }

                        Notificacoes.addLogConsole("ID " + idRetornado + " do cadastro de " + nomeCadastro + " excluído com sucesso");
                        Notificacoes.addLogConsole("*******************************************************************");
                    }
                });
            }
        }
    }

    public static int validarUrlAcessivel(String url) {
        int code = 0;

        try {
            URL acessaUrl = new URL(url);
            HttpURLConnection connection = (HttpURLConnection)acessaUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            code = connection.getResponseCode();
            connection.disconnect();
        } catch (IOException e) {
            Assert.fail("Não foi possível acessar a URL " + url);
        }

        return code;
    }
}
