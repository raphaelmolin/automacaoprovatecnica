package br.com.sicredi.tests.core.configuracao;

import br.com.sicredi.tests.ExecutaAutomacao;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Notificacoes extends ExecutaAutomacao {

    public static String horaAtual() {
        try {
            LocalDateTime agora = LocalDateTime.now();
            DateTimeFormatter formato = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss:SSSS",new Locale("pt", "BR"));
            String dataFormatada = formato.format(agora);
            return dataFormatada;
        } catch (Exception e) {
            return ("Não foi possível capturar a hora " + e);
        }
    }

    public static void addLogConsole(String mensagem) {
        System.out.println("[".concat(horaAtual()).concat("] ").concat(mensagem));
    }
}
