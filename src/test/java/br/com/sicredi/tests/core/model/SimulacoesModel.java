package br.com.sicredi.tests.core.model;

import java.math.BigDecimal;

public class SimulacoesModel {

	private String nome;
	private String cpf;
	private String email;
	private String valor;
	private String parcelas;
	private Boolean seguro;

	public static class Builder {

		private SimulacoesModel simulacoesModel;

		public Builder() {
			this.simulacoesModel = new SimulacoesModel();
		}

		public Builder(SimulacoesModel simulacoesModel) {
			this.simulacoesModel = simulacoesModel;
		}

		public static Builder create() {
			return new Builder();
		}

		public static Builder from(SimulacoesModel classes) {
			return new Builder(classes);
		}


		public void setSimulacoesModel(SimulacoesModel simulacoesModel) {
			this.simulacoesModel = simulacoesModel;
		}

		public Builder nome(String nome) {
			simulacoesModel.setNome(nome);
			return this;
		}

		public Builder cpf(String cpf) {
			simulacoesModel.setCpf(cpf);
			return this;
		}

		public Builder email(String email) {
			simulacoesModel.setEmail(email);
			return this;
		}

		public Builder valor(String valor) {
			simulacoesModel.setValor(valor);
			return this;
		}

		public Builder parcelas(String parcelas) {
			simulacoesModel.setParcelas(parcelas);
			return this;
		}

		public Builder seguro(Boolean seguro) {
			simulacoesModel.setSeguro(seguro);
			return this;
		}

		public SimulacoesModel build() {
			return simulacoesModel;
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getParcelas() {
		return parcelas;
	}

	public void setParcelas(String parcelas) {
		this.parcelas = parcelas;
	}

	public Boolean getSeguro() {
		return seguro;
	}

	public void setSeguro(Boolean seguro) {
		this.seguro = seguro;
	}
}
