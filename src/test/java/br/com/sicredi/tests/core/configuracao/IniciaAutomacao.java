package br.com.sicredi.tests.core.configuracao;

import br.com.sicredi.tests.ExecutaAutomacao;
import br.com.sicredi.tests.core.passos.PassosProjeto;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import org.junit.Assert;
import java.io.File;

public class IniciaAutomacao extends ExecutaAutomacao {

    public IniciaAutomacao() {
    }

    @Before(order = 0)
    public void before(Scenario cenario){
        auxiliar.setCenario(cenario);

        if (PassosProjeto.validarUrlAcessivel(Auxiliar.urlSimulacoes) > 400) {
            Notificacoes.addLogConsole("Código retornado: " + PassosProjeto.validarUrlAcessivel(Auxiliar.urlSimulacoes));
            Assert.fail("Não foi possível acessar a URL da Prova Técnica");
        }

    }

    public static void inicializarAutomacao(){
        auxiliar = new Auxiliar();
        auxiliar.setCaminhoExecucaoProjeto(new File(IniciaAutomacao.class
                .getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .getPath()
                .replaceAll("target", "")
                .replaceAll("classes", "")
                .replaceAll("test-", "")));

        auxiliar.inicializarParametros();

    }

}
