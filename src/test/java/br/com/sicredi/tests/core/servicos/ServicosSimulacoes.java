package br.com.sicredi.tests.core.servicos;

import br.com.sicredi.tests.ExecutaAutomacao;
import br.com.sicredi.tests.core.configuracao.Auxiliar;
import br.com.sicredi.tests.core.configuracao.Notificacoes;
import br.com.sicredi.tests.core.interfaces.IServicos;
import br.com.sicredi.tests.core.model.SimulacoesModel;
import br.com.sicredi.tests.core.passos.PassosProjeto;
import com.sun.jersey.api.client.ClientResponse;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.List;

public final class ServicosSimulacoes extends ExecutaAutomacao implements IServicos {

	private ServicosApiRest servicosApiRest;

	public ServicosSimulacoes(){
		servicosApiRest = new ServicosApiRest();
	}

	private void postSimulacoes(List<List<String>> listaTab) {
		ClientResponse client = null;
		SimulacoesModel simulacoes = null;

		for(int i = 1;i<listaTab.size();i++) {
			String nome = listaTab.get(i).get(0);
			String cpf = listaTab.get(i).get(1);
			String email = listaTab.get(i).get(2);
			String valor = listaTab.get(i).get(3);
			String parcelas = listaTab.get(i).get(4);
			String seguro = listaTab.get(i).get(5);

			if (nome.equals("")) { nome = null;}
			if (cpf.equals("")) { cpf = null;}
			if (email.equals("")) { email = null;}
			if (valor.equals("")) { valor = null;}
			if (parcelas.equals("")) { parcelas = null;}
			if (seguro.equals("")) { seguro = null;}

			simulacoes = SimulacoesModel.Builder.create()
					.nome(nome)
					.cpf(cpf)
					.email(email)
					.valor(valor)
					.parcelas(parcelas)
					.seguro(Boolean.valueOf(seguro))
					.build();

			client = servicosApiRest.realizarPost(Auxiliar.urlSimulacoes, new JSONObject(simulacoes));

			String retornoPost = client.getEntity(String.class);

			if (client.getStatus() != 201) {
				PassosProjeto.mensagemRetornada = retornoPost;
			}

			Notificacoes.addLogConsole("Cód. Status: " + client.getStatus());
			Notificacoes.addLogConsole("Retorno: " + retornoPost);
			PassosProjeto.statusRetorno = String.valueOf(client.getStatus());
		}
	}

	private void putSimulacoes(String identificador, List<List<String>> listaTab) {
		ClientResponse client = null;
		SimulacoesModel simulacao = null;

		for(int i = 1;i<listaTab.size();i++) {
			String nome = listaTab.get(i).get(0);
			String cpf = listaTab.get(i).get(1);
			String email = listaTab.get(i).get(2);
			String valor = listaTab.get(i).get(3);
			String parcelas = listaTab.get(i).get(4);
			String seguro = listaTab.get(i).get(5);

			simulacao = SimulacoesModel.Builder.create()
					.nome(nome)
					.cpf(cpf)
					.email(email)
					.valor(valor)
					.parcelas(parcelas)
					.seguro(Boolean.valueOf(seguro))
					.build();

			client = servicosApiRest.realizarPut(Auxiliar.urlSimulacoes, new JSONObject(simulacao), identificador);

			String retornoPut = client.getEntity(String.class);

			Notificacoes.addLogConsole("Cód. Status: " + client.getStatus());
			Notificacoes.addLogConsole("Retorno: " + retornoPut);
			PassosProjeto.statusRetorno = String.valueOf(client.getStatus());
		}
	}

	private void getSimulacoes(String identificador) {
		Notificacoes.addLogConsole("Realizando consulta do CPF: " + identificador);

		JSONObject retornoJson = servicosApiRest.realizarGet(Auxiliar.urlSimulacoes,identificador);

		retornoJson.keySet().forEach(keyStr ->	{
			Object keyvalue = retornoJson.get(keyStr);

			if (keyStr.equals("id")) {
				PassosProjeto.idRetornado = keyvalue.toString();
			}

			if (keyStr.equals("cpf")) {
				PassosProjeto.cpfRetornado = keyvalue.toString();
			}

			if (keyStr.equals("mensagem")) {
				PassosProjeto.mensagemRetornada = keyvalue.toString();
			}
		});

		PassosProjeto.simulacoesCadastradas = retornoJson.toString();
	}

	private void getSimulacoes() {
		Notificacoes.addLogConsole("Realizando consulta de todas as simulações");

		String retornoJson = servicosApiRest.realizarGet(Auxiliar.urlSimulacoes).toString();

		PassosProjeto.simulacoesCadastradas = retornoJson;
	}

	private void deleteSimulacoes(String identificador) {
		Notificacoes.addLogConsole("Realizando a exclusão do ID: " + identificador);
		servicosApiRest.realizarDelete(Auxiliar.urlSimulacoes,identificador);
	}

	public final void incluirSimulacoes(List<List<String>> listaTab) {
		postSimulacoes(listaTab);
	}

	public final void alterarSimulacao(String identificador, List<List<String>> listaTab) {
		putSimulacoes(identificador, listaTab);
	}

	public final void consultarSimulacoes(String identificador) {
		getSimulacoes(identificador);
	}

	public final void consultarSimulacoes() {
		getSimulacoes();
	}

	public final void excluirSimulacoes(String identificador) {
		deleteSimulacoes(identificador);
	}

	@Override
	public void postApiRest(List<List<String>> listaTab) {
		incluirSimulacoes(listaTab);
	}

	@Override
	public void putApiRest(String identificador, List<List<String>> listaTab) {
		alterarSimulacao(identificador, listaTab);
	}

	@Override
	public void deleteApiRest(String identificador) {
		excluirSimulacoes(identificador);
	}

	@Override
	public void getApiRest(String identificador) {
		consultarSimulacoes(identificador);
	}

	@Override
	public void getAllApiRest() {
		consultarSimulacoes();
	}


	
}
