package br.com.sicredi.tests.core.passos;

import br.com.sicredi.tests.ExecutaAutomacao;
import br.com.sicredi.tests.core.configuracao.Notificacoes;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class PassosSimulacoes extends ExecutaAutomacao {

    private final PassosProjeto passosProjeto;
    public static String idSimulacao;

    public PassosSimulacoes() {
        passosProjeto = new PassosProjeto();
    }

    @Before(value = "@Simulacoes")
    public void before(Scenario cenario) {
        auxiliar.setCenario(cenario);
        PassosProjeto.nomeCadastro = "Simulações";
        passosProjeto.limparSimulacoes();
    }

    @Given("^realizou a inclusão da simulação$")
    public void realizouInclusaoSimulacao(DataTable tabela){
        passosProjeto.incluirSimulacao(tabela);
    }

    @Given("^existam as simulações$")
    public void existamSimulacoes(DataTable tabela){

        passosProjeto.incluirSimulacao(tabela);
    }

    @When("^realizar a inclusão da simulação$")
    public void realizarInclusaoSimulacao(DataTable tabela){
        passosProjeto.incluirSimulacao(tabela);
    }

    @When("^o usuário consultar o CPF \"(.*?)\"$")
    public void consultarSimulacaoPorCpf(String filtro){
        passosProjeto.consultarSimulacao(filtro);
    }

    @When("^o usuário consultar todas as simulações$")
    public void consultarTodasSimulacoes(){
        passosProjeto.consultarSimulacoes();
    }

    @When("^o usuário alterar a simulação do CPF \"(.*?)\"$")
    public void alterarCampoSimulacao(String cpf, DataTable tabela){
        passosProjeto.consultarSimulacao(cpf);

        idSimulacao = PassosProjeto.idRetornado;

        if(!PassosProjeto.idRetornado.equals("")) {
            passosProjeto.alterarSimulacao(cpf, tabela);
        } else {
            Assert.fail("Não foi possível alterar a simulação. Mensagem retornada: " + PassosProjeto.mensagemRetornada);
        }
    }

    @When("^o usuário solicitar a remoção da simulação do CPF \"(.*?)\"$")
    public void removerSimulacaoPeloCPF(String filtro){
        passosProjeto.removerSimulacao(filtro);
    }

    @Then("^a simulação do CPF \"(.*?)\" será incluída com sucesso$")
    public void simulacaoRetornaStatus(String filtro){
        passosProjeto.consultarSimulacao(filtro);

        if (PassosProjeto.statusRetorno.equals("201") && !PassosProjeto.idRetornado.equals("")) {
            Notificacoes.addLogConsole("Incluída simulação ID " + PassosProjeto.idRetornado+ " para o CPF " + filtro);
            Notificacoes.addLogConsole("*******************************************************************");
        } else {
            Assert.fail("A simulação não foi incluída. Status retornado: " + PassosProjeto.statusRetorno);
        }
    }

    @Then("^a simulação não deve incluir o CPF \"(.*?)\"$")
    public void simulacaoNaoDeveIncluirCPF(String filtro){
        PassosProjeto.idRetornado = "";
        passosProjeto.consultarSimulacao(filtro);

        if (!PassosProjeto.idRetornado.equals("")) {
            Assert.fail("O sistema permitiu incluir uma simulação com CPF com restrição");
        }

        if (PassosProjeto.mensagemRetornada.equals("CPF " + filtro + "não encontrado")) {
            Notificacoes.addLogConsole("Mensagem retornada: " + PassosProjeto.mensagemRetornada);
            Notificacoes.addLogConsole("*******************************************************************");
        }
    }

    @Then("^a simulação não será inserida e apresentará a mensagem \"(.*?)\"$")
    public void retornarMensagem(String filtro){
        if (filtro.equals(PassosProjeto.mensagemRetornada)) {
            Notificacoes.addLogConsole("Mensagem retornada: " + PassosProjeto.mensagemRetornada);
            Notificacoes.addLogConsole("*******************************************************************");
        } else {
            if (PassosProjeto.statusRetorno.equals("201")) {
                Assert.fail("Registro foi inserido indevidamente. Mensagem esperada: " + filtro);
            }
            Assert.fail("Mensagem esperada: " + filtro + " - Mensagem retornada: " + PassosProjeto.mensagemRetornada);
        }
    }

    @Then("^o serviço deverá retornar as simulações cadastradas$")
    public void deveRetornarTodasSimulacoesCadastradas(){
        Notificacoes.addLogConsole(PassosProjeto.simulacoesCadastradas);
    }

    @Then("^o serviço deverá retornar a simulação cadastrada para o CPF \"(.*?)\"$")
    public void deveRetornarSimulacaoDoCpf(String filtro){
        if (filtro.equals(PassosProjeto.cpfRetornado)) {
            Notificacoes.addLogConsole("A simulação para o CPF " + filtro + " retornou o ID " + PassosProjeto.idRetornado);
            Notificacoes.addLogConsole("*******************************************************************");
        } else {
            Assert.fail("Não foi encontrada a simulação para o CPF " + filtro + " - Mensagem retornada: " + PassosProjeto.mensagemRetornada);
        }
    }

    @Then("^a simulação do CPF \"(.*?)\" deverá ser removida com sucesso$")
    public void deveRemoverSimulacao(String filtro){
        passosProjeto.consultarSimulacao(filtro);

        if (PassosProjeto.mensagemRetornada.equals("CPF " + filtro + " não encontrado")) {
            Notificacoes.addLogConsole("Simulação excluída com sucesso. Mensagem retornada: " + PassosProjeto.mensagemRetornada);
            Notificacoes.addLogConsole("*******************************************************************");
        } else {
            Assert.fail("A simulação do CPF " + filtro + " ainda consta no cadastro de simulações");
        }
    }

    @Then("^a simulação do CPF \"(.*?)\" deverá ser alterada com sucesso$")
    public void deveAlterarSimulacao(String filtro){
        if (PassosProjeto.statusRetorno.equals("200") && PassosProjeto.idRetornado.equals(idSimulacao)) {
            Notificacoes.addLogConsole("Incluída simulação ID " + PassosProjeto.idRetornado+ " para o CPF " + filtro);
            Notificacoes.addLogConsole("*******************************************************************");
        } else {
            Assert.fail("A simulação não foi incluída. Status retornado: " + PassosProjeto.statusRetorno);
        }
    }
}
