package br.com.sicredi.tests.core.configuracao;

import cucumber.api.Scenario;

import java.io.File;

public class Auxiliar {
    private static File caminhoExecucaoProjeto;
    private String diretorioPdf;
    private String diretorioVideo;
    private String diretorioImagensCenarios;
    private Scenario cenario;
    public static String urlRestricoes;
    public static String urlSimulacoes;


    public void setCaminhoExecucaoProjeto(File caminhoExecucao) {
        caminhoExecucaoProjeto = caminhoExecucao;
        this.diretorioPdf = caminhoExecucaoProjeto.getAbsolutePath() + "\\imagens\\pdfs";
        this.diretorioVideo = caminhoExecucaoProjeto.getAbsolutePath() + "\\videos";
        this.diretorioImagensCenarios = caminhoExecucaoProjeto.getAbsolutePath() + "\\imagens\\cenarios";
    }

    public void inicializarParametros(){
        urlRestricoes = "http://localhost:8888/api/v1/restricoes";
        urlSimulacoes = "http://localhost:8888/api/v1/simulacoes";
    }

    public void setCenario(Scenario cenario) {
        this.cenario = cenario;
    }

    public int getNumeroCenarios() {
        String texto = "";

        try {
            texto = this.cenario.getId();
        } catch (Exception var6) {
            texto = ";;;0;";
        }

        String[] partes = texto.split(";");
        boolean var3 = false;

        int numero;
        try {
            numero = Integer.parseInt(partes[3]) - 1;
        } catch (Exception var5) {
            numero = 1;
        }

        return numero;
    }

}
