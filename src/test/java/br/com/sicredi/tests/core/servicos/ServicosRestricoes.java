package br.com.sicredi.tests.core.servicos;

import br.com.sicredi.tests.ExecutaAutomacao;
import br.com.sicredi.tests.core.configuracao.Auxiliar;
import br.com.sicredi.tests.core.configuracao.Notificacoes;
import br.com.sicredi.tests.core.interfaces.IServicos;
import br.com.sicredi.tests.core.passos.PassosProjeto;
import org.json.JSONObject;


import java.util.List;

public final class ServicosRestricoes extends ExecutaAutomacao implements IServicos {

	private ServicosApiRest servicosApiRest;

	public ServicosRestricoes(){
		servicosApiRest = new ServicosApiRest();
	}

	private void getRestricoes(String identificador) {
		Notificacoes.addLogConsole("Realizando consulta do CPF: " + identificador);

		JSONObject retornoJson = servicosApiRest.realizarGet(Auxiliar.urlRestricoes,identificador);

		retornoJson.keySet().forEach(keyStr ->	{
			Object keyvalue = retornoJson.get(keyStr);
			if (keyStr.equals("mensagem")) {
				PassosProjeto.mensagemRetornada = keyvalue.toString();
			}
		});

		PassosProjeto.possuiRestricao = !PassosProjeto.mensagemRetornada.equals("");
	}

	public final void consultaRestricoes(String identificador) {
		getRestricoes(identificador);
	}

	@Override
	public void postApiRest(List<List<String>> listaTab) {
	}

	@Override
	public void putApiRest(String identificador, List<List<String>> listaTab) {
	}

	@Override
	public void deleteApiRest(String identificador) {
	}

	@Override
	public void getApiRest(String identificador) {
		consultaRestricoes(identificador);
	}

	@Override
	public void getAllApiRest() {
	}


	
}
