package br.com.sicredi.tests.core.servicos;

import br.com.sicredi.tests.ExecutaAutomacao;
import br.com.sicredi.tests.core.configuracao.Notificacoes;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import com.sun.jersey.api.client.*;
import com.sun.jersey.api.client.WebResource.Builder;

public final class ServicosApiRest extends ExecutaAutomacao {

	private String urlRest;

	public void setUrlRest(String urlRest) {
		this.urlRest = urlRest;
	}

	private void setUrlRestId(String urlRest, String id) {
		setUrlRest(urlRest.concat("/").concat(id));
	}

	public String getUrlRest() {
		return urlRest;
	}

	public ClientResponse realizarPost(String urlRest, Object jsonObj) {
		return realizaPost(urlRest, jsonObj);
	}

	public ClientResponse realizarPut(String urlRest, JSONObject jsonObj, String id) {
		return realizaPut(urlRest, jsonObj, id);
	}

	public ClientResponse realizarDelete(String urlRest, String id) {
		return realizaDelete(urlRest, id);
	}

	public JSONArray realizarGet(String urlRest) {
		return realizaGet(urlRest);
	}

	public JSONObject realizarGet(String urlRest, String id) {
		return realizaGet(urlRest, id);
	}

	private ClientResponse realizaPost(String urlRest, Object jsonPost) {
		setUrlRest(urlRest);
		ClientResponse response = null;
		try {
			Notificacoes.addLogConsole("Dados: ".concat(jsonPost.toString()));
			Notificacoes.addLogConsole("POST: ".concat(getUrlRest()));
			response = getResourceBuilder().post(ClientResponse.class, jsonPost.toString());

		} catch (UniformInterfaceException UIE) {
          if (Integer.toString(UIE.getResponse().getStatus()).equals("400")) {
              return UIE.getResponse();
          }
          System.err.println("POST - Código do erro: ".concat(Integer.toString(UIE.getResponse().getStatus())));
			System.err.println("POST - Mensagem erro: ".concat(UIE.getResponse().getEntity(String.class)));
		}
		return response;
	}

	private ClientResponse realizaPut(String urlRest, JSONObject jsonPut, String id) {
		setUrlRestId(urlRest, id);
		ClientResponse response = null;

		try {
			Notificacoes.addLogConsole("Dados: ".concat(jsonPut.toString()));
			Notificacoes.addLogConsole("PUT: ".concat(getUrlRest()));
			response = getResourceBuilder().put(ClientResponse.class, jsonPut.toString());

		} catch (UniformInterfaceException UIE) {
			System.err.println("PUT - Código do erro: ".concat(Integer.toString(UIE.getResponse().getStatus())));
			System.err.println("PUT - Mensagem do erro: ".concat(UIE.getResponse().getEntity(String.class)));
		}

		return response;
	}

	private ClientResponse realizaDelete(String urlRest, String id) {
		setUrlRestId(urlRest, id);
		ClientResponse response = null;
		try {
			Notificacoes.addLogConsole("DELETE: ".concat(getUrlRest()));
			response = getResourceBuilder().delete(ClientResponse.class);
			Notificacoes.addLogConsole("Mensagem: " + response.getEntity(String.class));
			return response;
		} catch (UniformInterfaceException UIE) {
			if (UIE.getResponse().getStatus() != 204) {
				System.err.println("Status: " + UIE.getResponse().getStatus());
				System.err.println("Mensagem: " + UIE.getResponse().getEntity(String.class));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	private JSONArray realizaGet(String urlRest) {
		setUrlRest(urlRest);
		String response = "";
		try {
			response = getResourceBuilder().get(String.class);
			return new JSONArray(response);
		} catch (UniformInterfaceException UIE) {
			System.err.println("Código do erro: ".concat(Integer.toString(UIE.getResponse().getStatus())));
			System.err.println("Mensagem erro: ".concat(UIE.getResponse().getEntity(String.class)));
		}
		return new JSONArray(response);
	}

	private JSONObject realizaGet(String urlRest, String id) {
		setUrlRestId(urlRest, id);
		String response = "";
		try {
			response = getResourceBuilder().get(String.class);
			return new JSONObject(response);
		} catch (UniformInterfaceException UIE) {
			if (Integer.toString(UIE.getResponse().getStatus()).equals("204")) {
				return new JSONObject();
			}
			if (Integer.toString(UIE.getResponse().getStatus()).equals("404")) {
				return new JSONObject(UIE.getResponse().getEntity(String.class));
			} else {
				System.err.println("Código do erro: ".concat(Integer.toString(UIE.getResponse().getStatus())));
				System.err.println("Mensagem erro: ".concat(UIE.getResponse().getEntity(String.class)));
			}
		}
		return new JSONObject(response);
	}

	private WebResource getClient() {
		return Client.create().resource(getUrlRest());
	}

	private Builder getResourceBuilder() {
		return getClient()
				.header("Content-Type", "application/json;charset=UTF-8")
				.header("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");
	}
}