package br.com.sicredi.tests.core.interfaces;

import java.util.List;

public interface IServicos {

	void postApiRest(List<List<String>> listaTab);

	void putApiRest(String identificador, List<List<String>> listaTab);

	void deleteApiRest(String identificador);

	void getApiRest(String identificador);

	void getAllApiRest();
}
