@Todos @Simulacoes
Feature: Remover uma simulação

@SimulacaoDeleteCenarioA
Scenario Outline: Realizar remoção de uma solicitação

Given existam as simulações
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A	| 00000000191	| clientea@teste.com	| 1000.00	| 2			| true		|
When o usuário solicitar a remoção da simulação do CPF "<CPF>"
Then a simulação do CPF "<CPF>" deverá ser removida com sucesso

Examples:
	| CPF			|
	| 00000000191	|