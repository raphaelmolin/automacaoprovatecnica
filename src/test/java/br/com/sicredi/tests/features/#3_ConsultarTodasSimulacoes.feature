@Todos @Simulacoes @ConsultaSimulacoes
Feature: Consultar todas as simulações

@SimulacoesGetAllCenarioA
Scenario: Realizar consulta de todas as simulações cadastradas

Given existam as simulações
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A	| 00000000191	| clientea@teste.com	| 1000.00	| 2			| true		|
	| Cliente B	| 00000000272	| clienteb@teste.com	| 10000.00	| 48		| false		|
When o usuário consultar todas as simulações
Then o serviço deverá retornar as simulações cadastradas

