@Todos @Simulacoes @CadastrarSimulacoes
Feature: Cadastrar uma simulação

@SimulacoesPostCenarioA
Scenario Outline: Realizar o cadastro de uma simulação com CPF sem restrição

Given o usuário consultou se o CPF "<CPF>" possui restrição
And o CPF não possua restrição
When realizar a inclusão da simulação
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A	| 00000000191	| clientea@teste.com	| 1000.00	| 2			| true		|
	| Cliente B	| 00000000272	| clienteb@teste.com	| 10000.00	| 48		| false		|
Then a simulação do CPF "<CPF>" será incluída com sucesso

Examples:
	| CPF			|
	| 00000000191	|
	| 00000000272	|


@SimulacoesPostCenarioB
Scenario Outline: Realizar o cadastro de uma simulação com CPF com restrição

Given realizou a inclusão da simulação
	| nome					| cpf			| email							| valor		| parcelas	| seguro	|
	| Cliente com restrição	| 97093236014	| clienterestricao@teste.com	| 5000.00	| 2			| true		|
And o CPF "<CPF>" possua restrição
Then a simulação não deve incluir o CPF "<CPF>"

Examples:
	| CPF			|
	| 97093236014	|

@SimulacoesPostCenarioC
Scenario Outline: Validar a inclusão de simulação sem informação no campo nome

Given realizou a inclusão da simulação
	| nome	| cpf			| email					| valor		| parcelas	| seguro	|
	|		| 00000000191	| clientea@teste.com	| 1000.00	| 2			| true		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem				 						|
	| {"erros":{"nome":"Nome não pode ser vazio"}}	|

@SimulacoesPostCenarioD
Scenario Outline: Validar a inclusão de simulação sem informação no campo cpf

Given realizou a inclusão da simulação
	| nome		| cpf	| email					| valor		| parcelas	| seguro	|
	| Cliente A |		| clientea@teste.com	| 1000.00	| 2			| true		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem				 						|
	| {"erros":{"cpf":"CPF não pode ser vazio"}}	|

@SimulacoesPostCenarioE
Scenario Outline: Validar a inclusão de simulação sem informação no campo email

Given realizou a inclusão da simulação
	| nome		| cpf			| email	| valor		| parcelas	| seguro	|
	| Cliente A | 00000000191	|		| 1000.00	| 2			| true		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem					 						|
	| {"erros":{"email":"E-mail não deve ser vazio"}}	|

@SimulacoesPostCenarioF
Scenario Outline: Validar a inclusão de simulação sem informação no campo valor

Given realizou a inclusão da simulação
	| nome		| cpf			| email					| valor	| parcelas	| seguro	|
	| Cliente A | 00000000191	| clientea@teste.com	|		| 2			| true		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem					 						|
	| {"erros":{"valor":"Valor não pode ser vazio"}}	|

@SimulacoesPostCenarioG
Scenario Outline: Validar a inclusão de simulação sem informação no campo parcelas

Given realizou a inclusão da simulação
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A | 00000000191	| clientea@teste.com	| 1000.00	| 			| true		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem					 							|
	| {"erros":{"parcelas":"Parcelas não pode ser vazio"}}	|

@SimulacoesPostCenarioH
Scenario Outline: Validar a inclusão de simulação com valor inferior a R$ 1000,00

Given realizou a inclusão da simulação
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A | 00000000191	| clientea@teste.com	| 100.00	| 2			| false		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem					 										|
	| {"erros":{"valor":"Valor deve ser maior ou igual a R$ 1.000"}}	|

@SimulacoesPostCenarioI
Scenario Outline: Validar a inclusão de simulação com valor superior a R$ 40000,00

Given realizou a inclusão da simulação
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A | 00000000191	| clientea@teste.com	| 50000.00	| 2			| true		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem					 										|
	| {"erros":{"valor":"Valor deve ser menor ou igual a R$ 40.000"}}	|

@SimulacoesPostCenarioJ
Scenario Outline: Validar a inclusão de simulação com email inválido

Given realizou a inclusão da simulação
	| nome		| cpf			| email			| valor		| parcelas	| seguro	|
	| Cliente A | 00000000191	| emailinvalido	| 5000.00	| 2			| true		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem					 								|
	| {"erros":{"email":"E-mail deve ser um e-mail válido"}}	|

@SimulacoesPostCenarioK
Scenario Outline: Validar a inclusão de simulação com número de parcela inferior a 2

Given realizou a inclusão da simulação
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A | 00000000191	| clientea@teste.com	| 5000.00	| 1			| true		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem					 										|
	| {"erros":{"parcelas":"Parcelas deve ser igual ou maior que 2"}}	|

@SimulacoesPostCenarioL
Scenario Outline: Validar a inclusão de simulação com número de parcela superior a 48

Given realizou a inclusão da simulação
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A | 00000000191	| clientea@teste.com	| 5000.00	| 50		| true		|
Then a simulação não será inserida e apresentará a mensagem "<mensagem>"

Examples:
	| mensagem					 										|
	| {"erros":{"parcelas":"Parcelas deve ser igual ou menor que 48"}}	|