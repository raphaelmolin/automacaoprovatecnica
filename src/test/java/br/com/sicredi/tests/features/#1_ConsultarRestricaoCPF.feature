@Todos @Restricoes
Feature: Consultar um CPF

@RestricoesCenarioA
Scenario Outline: Realizar consulta de um CPF para identificar se possui restrição

Given o usuário consultou se o CPF "<CPF>" possui restrição
Then o serviço deverá retornar a mensagem "<mensagem>"

Examples:
	| CPF			| mensagem							|
	| 97093236014	| O CPF 97093236014 tem problema	|
	| 60094146012	| O CPF 60094146012 tem problema	|
	| 84809766080	| O CPF 84809766080 tem problema	|
	| 62648716050	| O CPF 62648716050 tem problema	|
	| 26276298085	| O CPF 26276298085 tem problema	|
	| 01317496094	| O CPF 01317496094 tem problema	|
	| 55856777050	| O CPF 55856777050 tem problema	|
	| 19626829001	| O CPF 19626829001 tem problema	|
	| 24094592008	| O CPF 24094592008 tem problema	|
	| 58063164083	| O CPF 58063164083 tem problema	|
	| 11111111111	|									|
	| 1111111		|									|
	| ABC			|									|
