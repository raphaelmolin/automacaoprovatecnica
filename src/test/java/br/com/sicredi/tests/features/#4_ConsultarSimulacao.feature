@Todos @Simulacoes @ConsultaSimulacoes
Feature: Consultar simulação pelo CPF

@SimulacaoGetCenarioA
Scenario Outline: Realizar consulta de uma simulação pelo CPF

Given existam as simulações
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A	| 00000000191	| clientea@teste.com	| 1000.00	| 2			| true		|
	| Cliente B	| 00000000272	| clienteb@teste.com	| 10000.00	| 48		| false		|
When o usuário consultar o CPF "<CPF>"
Then o serviço deverá retornar a simulação cadastrada para o CPF "<CPF>"

Examples:
	| CPF			|
	| 00000000191	|
	| 00000000272	|