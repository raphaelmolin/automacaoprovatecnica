@Todos @Simulacoes
Feature: Alterar uma simulação

@SimulacaoPutCenarioA
Scenario Outline: Realizar a alteração de uma simulação

Given existam as simulações
	| nome		| cpf			| email					| valor		| parcelas	| seguro	|
	| Cliente A	| 00000000191	| clientea@teste.com	| 1000.00	| 2			| true		|
When o usuário alterar a simulação do CPF "<CPF>"
	| nome					| cpf			| email						| valor		| parcelas	| seguro	|
	| Cliente A Alterado	| 00000000191	| clientealterado@teste.com	| 5000.00	| 12		| false		|
Then a simulação do CPF "<CPF>" deverá ser alterada com sucesso

Examples:
	| CPF			|
	| 00000000191	|